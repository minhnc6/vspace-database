package vn.vspace.database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vn.vspace.database.jpa.Clip;
import vn.vspace.database.service.ClipService;

import java.util.List;

@RestController
@RequestMapping(path = "/clips")
public class ClipController {

    @Autowired
    ClipService clipService;

    @PostMapping(path = "/add")
    public Clip addApiMappingLog(@RequestBody Clip clip) {
        Clip insertedLog = clipService.create(clip);
        return insertedLog;
    }

    @GetMapping("/get-all")
    public List<Clip> getTransId(){
        return clipService.findAll();
    }
}
