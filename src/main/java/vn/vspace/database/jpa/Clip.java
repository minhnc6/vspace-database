package vn.vspace.database.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Entity // Đánh dấu đây là table trong db
@Data   // lombok giúp generate các hàm constructor, get, set v.v.
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "yos_clip")
public class Clip implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    private String url;

    private String title;

    private String description;

    private int type;

    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "last_modified_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedTime;

    @Column(name = "published_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date publishedTime;

    private String thumbnail;

    @Column(name = "like_count")
    private Long likeCount;

    @Column(name = "view_count")
    private Long viewCount;

    private int feature;

    private int status;

    private Time duration;

    @PrePersist
    public void prePersist(){
        if(this.createdTime == null){
            this.createdTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }

    @PreUpdate
    public void preUpdate(){
        if(this.lastModifiedTime == null){
            this.lastModifiedTime = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }
    }
}
