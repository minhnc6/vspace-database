package vn.vspace.database.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.vspace.database.jpa.Clip;

@Repository
public interface ClipRepo extends JpaRepository<Clip, Long> {

}
