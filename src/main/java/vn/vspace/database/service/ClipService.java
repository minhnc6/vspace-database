package vn.vspace.database.service;

import vn.vspace.database.jpa.Clip;

import java.util.List;

public interface ClipService {
    List<Clip> findAll();
    Clip create(Clip clip);
}
