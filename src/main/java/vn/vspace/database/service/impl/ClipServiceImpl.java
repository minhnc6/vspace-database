package vn.vspace.database.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.vspace.database.jpa.Clip;
import vn.vspace.database.repo.ClipRepo;
import vn.vspace.database.service.ClipService;

import java.util.List;

@Service
public class ClipServiceImpl implements ClipService {

    @Autowired
    ClipRepo clipRepo;

    @Override
    public List<Clip> findAll() {
        return clipRepo.findAll();
    }

    @Override
    public Clip create(Clip clip) {
        return clipRepo.save(clip);
    }
}
